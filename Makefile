SHELL = /bin/bash

base_image = debian:buster
build_tag ?= debian-buster

packages = curl gosu less locales nano ncat vim wait-for-it

.PHONY: build
build:
	docker pull $(base_image)
	DOCKER_BUILDKIT=1 docker build -t $(build_tag) \
		--build-arg base_image=$(base_image) \
		--build-arg packages="$(packages)" \
		./src

.PHONY: clean
clean:
	echo 'no-op'

.PHONY: test
test:
	docker run --rm $(build_tag) /bin/sh -c 'echo "Hello, world!"'
