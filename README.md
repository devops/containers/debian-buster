# Debian buster base image

Based on official Debian buster Docker image.

Adds common packages, sets locale and timezone.
